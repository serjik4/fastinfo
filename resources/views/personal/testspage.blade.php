<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- <base href="/"> -->
    <title>FastInfo</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="" />
   <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script:700" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('css/front.css')}}">
</head>
<body>
<div class="wrapper">
    
    <a href="https://www.t.me/abo_ziad95" class="telega-link"><img src="{{asset('img/telegram_icon.svg')}}" alt="#"></a>
    <!-- header -->
    <header class="header">
        <div class="container header-wrap">
            <a href="{{url('/')}}" class="header-logo">
                FastInfo
            </a>
            <div class="info">
                <span class="info-item"><a href="{{route('personal.dashboard')}}" class="info-item-link">Личный кабинет</a></span>
                <span class="info-item">{{Auth::user()->email}}</span>
                {{--<span class="info-item">{{Auth::user()->faculty}}</span>--}}
            </div>
            <nav class="header-nav personal">
                <a href="{{route('logout')}}" class="header-nav-btn personal">Выйти <img src="{{asset('img/exit.svg')}}" alt="#" class="personal-logout-img"></a>
            </nav>
            <div class="nav-mob-humburger">
                <span class="nav-mob-huburger-span"></span>
                <span class="nav-mob-huburger-span"></span>
                <span class="nav-mob-huburger-span"></span>
            </div>
        </div>
    </header>
    <!-- // header -->
    <nav class="nav-mob">
        <div class="nav-mob-list">

            <a href="{{route('personal.dashboard')}}" class="header-nav-btn-mob">Личный кабинет</a></span>
            <a href="{{route('logout')}}" class="header-nav-btn-mob">Выйти</a>
        </div>
        <div class="info info-mob"><!-- 
            <span class="info-item"><a href="{{route('personal.dashboard')}}">Personal</a></span> -->
            <span class="info-item">{{Auth::user()->email}}</span>
            {{--<span class="info-item">{{Auth::user()->faculty}}</span>--}}
        </div>
    </nav>
    <!-- // header -->
    <section class="testpage">
        <!-- begin loader -->
        <div class="main-loader">
            <div class="helix">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
        <!-- end loader -->
        <div class="container">
            <div class="personal-btn-items flex-column">
                <button class="btn personal-btn-item btn-s js-btn-tab active" data-atributte="search">поиск</button>
                <button class="btn personal-btn-item btn-s js-btn-tab" data-atributte="test">начать тест</button>
                <button class="btn personal-btn-item btn-s js-btn-tab" data-atributte="testModul">начать модульный тест</button>
            </div>
            <div class="test-page-content active" data-atributte="search">
                <input id="js-search-input" class="personal-input-search" type="text" data-bookid="{{$book->id}}" placeholder="search with this book">
                <div class="js-search-content">
 <!--                    <div class="test-page-conten-search">
                        <span class="test-page-conten-search-q">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque ratione commodi, nobis culpa magni porro obcaecati a inventore vel et sint unde. Molestias deleniti et quo natus ipsum sint perferendis.</span>
                        <span class="test-page-conten-search-a">smdlasjdashash hsfaigf uasfuh aiafs fa asuiasf fasadf </span>
                    </div>
                    <div class="test-page-conten-search">
                        <span class="test-page-conten-search-q">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque ratione commodi, nobis culpa magni porro obcaecati a inventore vel et sint unde. Molestias deleniti et quo natus ipsum sint perferendis.</span>
                        <span class="test-page-conten-search-a">smdlasjdashash hsfaigf uasfuh aiafs fa asuiasf fasadf </span>
                    </div>
                    <div class="test-page-conten-search">
                        <span class="test-page-conten-search-q">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque ratione commodi, nobis culpa magni porro obcaecati a inventore vel et sint unde. Molestias deleniti et quo natus ipsum sint perferendis.</span>
                        <span class="test-page-conten-search-a">smdlasjdashash hsfaigf uasfuh aiafs fa asuiasf fasadf </span>
                    </div>
                    <div class="test-page-conten-search">
                        <span class="test-page-conten-search-q">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque ratione commodi, nobis culpa magni porro obcaecati a inventore vel et sint unde. Molestias deleniti et quo natus ipsum sint perferendis.</span>
                        <span class="test-page-conten-search-a">smdlasjdashash hsfaigf uasfuh aiafs fa asuiasf fasadf </span>
                    </div>
                    <div class="test-page-conten-search">
                        <span class="test-page-conten-search-q">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque ratione commodi, nobis culpa magni porro obcaecati a inventore vel et sint unde. Molestias deleniti et quo natus ipsum sint perferendis.</span>
                        <span class="test-page-conten-search-a">smdlasjdashash hsfaigf uasfuh aiafs fa asuiasf fasadf </span>
                    </div> -->
                </div>
            </div>
            <div class="test-page-content" data-atributte="test">
                <p class="dropdown-title">выбирите тему</p>
                <div class="dropdown">
                    <button class="dropdown-toggle dropdown-toggle-btn" type="button" data-toggle="dropdown"><span class="js-select-search-input">выбирите тему</span>
                        <span class="caret"></span></button>
                    <ul class="dropdown-menu dropdown-menu-ul">
                        <input class="form-control" id="myInput" type="text" placeholder="Search..">
                        @foreach($topics as $topic)
                            <li><a href="#" class="js-select-search-btn" data-category="{{$topic->id}}" data-name="{{$topic->topic}}">{{$topic->topic}}</a></li>
                        @endforeach
                    </ul>
                </div>
                <div class="test-page-content-test-count">
                    <div class="test-page-content-test-counter-wrap">
                        <span class="test-page-content-test-counter js-test-page-content-test-counter-all">0</span>
                        <span class="test-page-content-test-text">всего вопросов</span>
                    </div>
                    <div class="test-page-content-test-counter-wrap">
                        <span class="test-page-content-test-counter js-test-page-content-test-counter">0</span>
                        <span class="test-page-content-test-text">пройденых вопросов</span>
                    </div>
                </div>
<div class="js-test-page-content-test ">
<!--                 <div class="test-page-content-test">
                    <div class="test-page-content-test-q">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque ratione commodi, nobis culpa magni porro obcaecati a???</div>
                    <ul>
                        <li class="test-page-content-li red"><input type="radio" name="Q" id="Q1"><label for="Q1">Q1dfbfgdfg</label></li>
                        <li class="test-page-content-li green"><input type="radio" name="Q" id="Q2"><label for="Q2">Q2saddsadsadsaadsad</label></li>
                        <li class="test-page-content-li"><input type="radio" name="Q" id="Q3"><label for="Q3">Q3asdsadadsdsa</label></li>
                        <li class="test-page-content-li red"><input type="radio" name="Q" id="Q4"><label for="Q4">Q4asdsadasdasdasdasd</label></li>
                    </ul>
                </div> -->
                </div>
                <div class="personal-btn-items under">
                    <button class="btn personal-btn-item btn-s js-test-check">Подсказка</button>
                    <button class="btn personal-btn-item btn-s js-test-next">Следующий вопрос</button> 
                </div>
            </div>
            <div class="test-page-content" data-atributte="testModul">
                <div class="js-module-content">
<!--                 <div class="test-page-content-test">
                    <div class="test-page-content-test-q">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque ratione commodi, nobis culpa magni porro obcaecati a???</div>
                    <ul>
                        <li class="test-page-content-li"><input type="radio" name="Q" id="MQ1"><label for="MQ1">Q1dasdasdasdasdsa</label></li>
                        <li class="test-page-content-li"><input type="radio" name="Q" id="MQ2"><label for="MQ2">Q2safsadsf</label></li>
                        <li class="test-page-content-li"><input type="radio" name="Q" id="MQ3"><label for="MQ3">Qsfaff3</label></li>
                        <li class="test-page-content-li"><input type="radio" name="Q" id="MQ4"><label for="MQ4">Q4dsaassaddsadsa</label></li>
                    </ul>
                </div> -->
            </div>
                <div class="personal-btn-items under">
                    <button class="btn personal-btn-item btn-s js-module-test-check">Подсказка</button>
                    <button class="btn personal-btn-item btn-s js-module-test-next">Следующий вопрос</button>
                </div>
            </div>
        </div>
    </section>
    <!-- //testpage -->
    <div class="modal-wrap js-modal-reg">
        <div class="modal">
            <div class="container container-modal">
                <div class="modal-desc">
                    <button class="modal-back">X</button>
                    <div class="modal-desc-title">регестрация</div>
                    <div class="modal-desc-subtitle"></div>
                    <form class="modal-form" action="#">
                        <div class="modal-inputs">
                            <input type="text" class="modal-Name modal-input" placeholder="Your full name">
                            <input type="text" class="modal-Faculty modal-input" placeholder="Faculty">
                            <input type="text" class="modal-email modal-input" placeholder="Your Email">
                            <input type="text" class="modal-pass modal-input" placeholder="password">
                        </div>
                        <button class="modalbutton" type="submit">Регестрация</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- // modal -->
    <div class="modal-wrap js-modal-reg">
        <div class="modal-main">
            <div class="container container-modal">
                <div class="modal-desc">
                    <button class="modal-back">X</button>
                    <div class="modal-desc-title">Регестрация</div>
                    <form class="modal-form" action="#">
                        <div class="modal-inputs">
                            <input type="text" class="modal-Name modal-input" placeholder="Your full name">
                            <input type="text" class="modal-Faculty modal-input" placeholder="Faculty">
                            <input type="text" class="modal-email modal-input" placeholder="Your Email">
                            <input type="text" class="modal-pass modal-input" placeholder="password">
                        </div>
                        <button class="modalbutton" type="submit">Регестрация</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- // modal -->
    <div class="modal-wrap js-modal-login">
        <div class="modal-main">
            <div class="container container-modal">
                <div class="modal-desc">
                    <button class="modal-back">X</button>
                    <div class="modal-desc-title">Вход</div>
                    <form class="modal-form" action="#">
                        <div class="modal-inputs">
                            <input type="text" class="modal-email modal-input" placeholder="Your Email">
                            <input type="text" class="modal-pass modal-input" placeholder="password">
                        </div>
                        <button class="modalbutton" type="submit">Вход</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- // modal -->
    <footer class="footer">
        <div class="container footer-wrap test-page">
            <div class="footer-logo">
                <a href="{{url('/')}}" class="header-logo">
                    FastInfo
                </a>
            </div>
            <div class="footer-links">
                <a href="{{route('logout')}}" class="header-nav-btn">Выйти</a>
            </div>
        </div>
        <!-- // container -->
    </footer>
    <!-- // footer -->
</div>
<!-- // wrapper -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="{{asset('js/front.js')}}"></script>
</body>
</html>