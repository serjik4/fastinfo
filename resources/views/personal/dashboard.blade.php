<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- <base href="/"> -->
    <title>FastInfo</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="" />
   <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script:700" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/front.css')}}">
</head>
<body>
<div class="wrapper">
    <a href="https://www.t.me/abo_ziad95" class="telega-link"><img src="{{asset('img/telegram_icon.svg')}}" alt="#"></a>
    <!-- header -->
    <header class="header">
        <div class="container header-wrap">
            <a href="{{url('/')}}" class="header-logo">
                FastInfo
            </a>
            <div class="info">
                <span class="info-item">{{Auth::user()->email}}</span>
                {{--<span class="info-item">{{Auth::user()->faculty}}</span>--}}
            </div>
            <nav class="header-nav personal">
                <a href="{{route('logout')}}" class="header-nav-btn personal">Выйти <img src="{{asset('img/exit.svg')}}" alt="#" class="personal-logout-img"></a>
            </nav>
            <div class="nav-mob-humburger">
                <span class="nav-mob-huburger-span"></span>
                <span class="nav-mob-huburger-span"></span>
                <span class="nav-mob-huburger-span"></span>
            </div>
        </div>
    </header>
    <!-- // header -->
    <nav class="nav-mob">
        <div class="nav-mob-list">
            <a href="{{route('logout')}}" class="header-nav-btn-mob">Выйти</a>
        </div>
        <div class="info info-mob">
            <span class="info-item">{{Auth::user()->email}}</span>
            {{--<span class="info-item">{{Auth::user()->faculty}}</span>--}}
        </div>
    </nav>
    <!-- // nav -->
    <section class="personal">
        <!-- begin loader -->
        <div class="main-loader">
            <div class="helix">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
        <!-- end loader -->
        <div class="container">
            <div class="personal-btn-items">
                <button class="btn personal-btn-item js-btn-tab" data-atributte="books">доступные</button>
                <button class="btn personal-btn-item active js-btn-tab" data-atributte="material">материал</button>
                {{--<button class="btn personal-btn-item js-btn-tab">результаты</button>--}}
            </div>
            <div class="content">
                <div class="test-page-content active" data-atributte="material">
                <h3 class="books-title">
                    Учебный материал
                    <span class="books-title-span">для получение бесплатного доступа на два часа связивайтесь с нами в <a href="https://www.t.me/abo_ziad95" class="modal-desc-info-link" target="_blank">Telegram</a>:)</span>

                </h3>
                <div class="books-items">
                    @foreach($books as $book)
                        <div class="books-item">
                            <div class="books-item-img">
                              <img src="{{$book->getImage()}}" alt="">
                            </div>
                            <div class="books-item-title">
                              {{$book->name}}
                            </div>
                            <div class="books-item-price">
                                Цена:
                                <span>{{$book->price}}</span> грн.
                            </div>
                            <div class="books-item-desc">
                              {{--{{$book->description}}--}}
                            </div>
                            <button class="btn books-btn js-book-requst" data-bookname= "{{$book->name}}" >ПОЛУЧИТЬ ДОСТУП</button>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="test-page-content" data-atributte="books">
                <h3 class="books-title">
                    доступные
                    <span class="books-title-span">здесь будут отброжаться все доступные для вас книги</span>
                </h3>
                <div class="books-items">
                    @if($available_books != null)
                        @foreach($available_books as $available_book)
                        <div class="books-item">
                            <div class="books-item-img">
                                <img src="{{$available_book->getImage()}}" alt="">
                            </div>
                            <div class="books-item-title">
                                {{$available_book->name}}
                            </div>
                            <a href="{{route('personal.tests', ['id' => $available_book->id])}}" class="btn books-btn-a books-btn personal">начать тест</a>
                            <a target="_blank" href="{{url(sprintf('book/%s', $available_book->filename))}}" class="btn books-btn-a books-btn personal">открыт книгу</a>
                        </div>
                        @endforeach
                    @endif
                </div>
            </div>
            </div>
            <!-- // content -->
        </div>
        <!-- // container -->
    </section>
    <!-- //personal -->
    <div class="modal-wrap js-modal-reg">
        <div class="modal-main">
            <div class="container container-modal">
                <div class="modal-desc">
                    <button class="modal-back">X</button>
                    <div class="modal-desc-title">Регистрация</div>
                    <form class="modal-form" action="{{route('register')}}" method="POST">
                        <div class="modal-inputs">
                            @csrf
                            <input type="text" name="name" class="modal-Name modal-input" placeholder="Your full name" required>
                            <input type="text" name="faculty" class="modal-Faculty modal-input" placeholder="Faculty" required>
                            <input type="email" name="email" class="modal-email modal-input" placeholder="Your Email" required>
                            <input type="password" name="password" class="modal-pass modal-input" placeholder="password" required>
                        </div>
                        <button class="modalbutton" type="submit">Регистрация</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- // modal -->
    <div class="modal-wrap js-modal-login">
        <div class="modal-main">
            <div class="container container-modal">
                <div class="modal-desc">
                    <button class="modal-back">X</button>
                    <div class="modal-desc-title">Вход</div>
                    <form class="modal-form" action="{{route('login')}}" method="POST">
                        <div class="modal-inputs">
                            @csrf
                            <input type="email" name="email" class="modal-email modal-input" placeholder="Your Email" required>
                            <input type="password" name="password" class="modal-mobile modal-input" placeholder="Your password" required>
                        </div>
                        <button class="modalbutton" type="submit">Вход</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- // modal -->
    <div class="modal-wrap js-modal-requst">
        <div class="modal-main">
            <div class="container container-modal">
                <div class="modal-desc">
                    <button class="modal-back">X</button>
                    <div class="modal-desc-title js-modal-desc-title-requst"></div>
                    <div class="modal-desc-info">
                        для получение доступа для этой книге есть два способа оплаты
                        <ul class="modal-desc-info-ul">
                            <li class="modal-desc-info-li"><span>1.</span>наличный расчет <a href="https://www.t.me/abo_ziad95" class="modal-desc-info-link" target="_blank">напишите нам в Telegram</a> чтоб договорится где и как :)</li>
                            <li class="modal-desc-info-li"><span>2.</span>банковский перевод на карту <a href="https://www.t.me/abo_ziad95" class="modal-desc-info-link" target="_blank">напишите нам в Telegram</a> для получение реквизитов :)</li>
                        </ul>
                        <span class="modal-desc-info-span">P.S:<a href="https://www.t.me/abo_ziad95" class="modal-desc-info-link" target="_blank">напишите нам в Telegram</a> для получение бесплатного доступа на два часа</span>
                     </div>
                </div>
            </div>
        </div>
    </div>
    <!-- // modal -->
    <footer class="footer">
        <div class="container footer-wrap test-page">
            <div class="footer-logo">
                <a href="{{url('/')}}" class="header-logo">
                    FastInfo
                </a>
            </div>
            <div class="footer-links">
                <a href="{{route('logout')}}" class="header-nav-btn">Выйти</a>
            </div>
        </div>
        <!-- // container -->
    </footer>
    <!-- // footer -->
</div>
<!-- // wrapper -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="{{asset('js/front.js')}}"></script>

</body>
</html>