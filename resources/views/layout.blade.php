<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- <base href="/"> -->
    <title>FastInfo</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}" type="image/x-icon">
    <meta name="description" content="" />
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script:700" rel="stylesheet">
    {{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/css/bootstrap-select.min.css">--}}
    <link rel="stylesheet" href="{{asset('css/front.css')}}">
</head>
<body>
<div class="wrapper">
    
    <a href="https://www.t.me/abo_ziad95" class="telega-link">
        <img src="{{asset('img/telegram_icon.svg')}}" alt="#">
    </a>
    <!-- header -->
    <header class="header main">
        <div class="container header-wrap">
            <a href="{{url('/')}}" class="header-logo">
                FastInfo
            </a>
            @if(Auth::check())
                <nav class="header-nav">
                    <a href="{{route('personal.dashboard')}}" class="header-nav-btn">{{Auth::user()->email}}</a>
                    <a href="{{route('logout')}}" class="header-nav-btn">Выйти</a>
                </nav>
            @else
                <nav class="header-nav">
                    <a href="#" class="header-nav-btn js-login">Вход</a>
                    <a href="#" class="header-nav-btn js-create">Регистрация</a>
                </nav>
            @endif
            <div class="nav-mob-humburger">
                <span class="nav-mob-huburger-span"></span>
                <span class="nav-mob-huburger-span"></span>
                <span class="nav-mob-huburger-span"></span>
            </div>
        </div>
    </header>
    <!-- // header -->
    <nav class="nav-mob">
        @if(Auth::check())
            <div class="nav-mob-list">
                <a href="{{route('personal.dashboard')}}" class="header-nav-btn-mob">{{Auth::user()->email}}</a>
                <a href="{{route('logout')}}" class="header-nav-btn-mob">Выйти</a>
            </div>
        @else
            <div class="nav-mob-list">
                <a href="#" class="header-nav-btn-mob js-login">Вход</a>
                <a href="#" class="header-nav-btn-mob js-create">Регистрация</a>
            </div>
        @endif
    </nav>
    @include('errors')
    <!-- // header -->
    @yield('content')
    <!-- // books -->
    <div class="modal-wrap js-modal-reg">
        <div class="modal-main">
            <div class="container container-modal">
                <div class="modal-desc">
                    <button class="modal-back">X</button>
                    <div class="modal-desc-title">Регистрация</div>
                    <form class="modal-form" action="{{route('register')}}" method="POST">
                        <div class="modal-inputs">
                            @csrf
                            <input type="text" name="name" class="modal-Name modal-input" placeholder="Фамилия Имя" required>
                            <input type="text" name="faculty" class="modal-Faculty modal-input" placeholder="факультет" required>
                            <input type="text" name="phone" class="modal-telegram modal-input" placeholder="Telegram или номер телефона" required>
                            <input type="email" name="email" class="modal-email modal-input" placeholder="Email" required>
                            <input type="password" name="password" class="modal-pass modal-input" placeholder="пароль" required>
                        </div>
                        <button class="modalbutton" type="submit">Регистрация</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- // modal -->
    <div class="modal-wrap js-modal-login">
        <div class="modal-main">
            <div class="container container-modal">
                <div class="modal-desc">
                    <button class="modal-back">X</button>
                    <div class="modal-desc-title">Вход</div>
                    <form class="modal-form" action="{{route('login')}}" method="POST">
                        <div class="modal-inputs">
                            @csrf
                            <input type="email" name="email" class="modal-email modal-input" placeholder="Email" required>
                            <input type="password" name="password" class="modal-mobile modal-input" placeholder="пароль" required>
                        </div>
                        <button class="modalbutton" type="submit">Вход</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- // modal -->
    <div class="modal-wrap js-modal-requst">
        <div class="modal-main">
            <div class="container container-modal">
                <div class="modal-desc">
                    <button class="modal-back">X</button>
                    <div class="modal-desc-title js-modal-desc-title-requst"></div>
                    <div class="modal-desc-info">
                        для получение доступа для этой книге есть два способа оплаты
                        <ul class="modal-desc-info-ul">
                            <li class="modal-desc-info-li"><span>1.</span>наличный расчет <a href="https://www.t.me/abo_ziad95" class="modal-desc-info-link" target="_blank">напишите нам в Telegram</a> чтоб договорится где и как :)</li>
                            <li class="modal-desc-info-li"><span>2.</span>банковский перевод на карту <a href="https://www.t.me/abo_ziad95" class="modal-desc-info-link" target="_blank">напишите нам в Telegram</a> для получение реквизитов :)</li>
                        </ul>
                        <span class="modal-desc-info-span">P.S:<a href="https://www.t.me/abo_ziad95" class="modal-desc-info-link" target="_blank">напишите нам в Telegram</a> для получение бесплатного доступа на два часа</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- // modal -->
    
    <footer class="footer">
        <div class="container footer-wrap">
            <div class="footer-logo">
                <a href="#" class="header-logo">
                    FastInfo
                </a>
            </div>
            @if(!Auth::check())
                <div class="footer-links">
                    <a href="#" class="header-nav-btn js-login">Вход</a>
                    <a href="#" class="header-nav-btn js-create">Регистрация</a>
                </div>
            @endif
        </div>
        <!-- // container -->
    </footer>
    <!-- // footer -->
</div>
<!-- // wrapper -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/js/bootstrap-select.min.js"></script>--}}
<script src="{{asset('js/front.js')}}"></script>
{{--<script src="js/ui.js"></script>--}}
{{--<script src="js/books.js"></script>--}}
{{--<script src="js/app.js"></script>--}}
</body>
</html>