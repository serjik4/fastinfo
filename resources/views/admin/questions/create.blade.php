@extends('admin.layout')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Добавить вопрос
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
        {{Form::open(['method' => 'post', 'route' => 'questions.store', 'files' => 'true'])}}
        <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Добавляем вопрос</h3>
                    @include('admin.errors')
                </div>
                <div class="box-body">
                    <div class="col-md-6">

                        <div class="form-group">
                            <label for="exampleInputDescription1">Question</label>
                            <textarea name="question" id="" cols="30" rows="7" class="form-control" id="exampleInputDescription1" placeholder="Question">{{old('question')}}</textarea>
                        </div>
                        <div class="form-group">
                            <label>Topic</label>
                            {{Form::select(
                                'topic_id',
                                $topics,
                                null,
                                ['class' => 'form-control select2']
                            )}}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputName1">Answers</label>
                            <input type="text" name="answers[]" class="form-control" id="exampleInputName1" placeholder="Type answer">
                            <input type="radio" name="is_correct" value="0" checked>
                            <input type="text" name="answers[]" class="form-control" id="exampleInputName1" placeholder="Type answer">
                            <input type="radio" name="is_correct" value="1">
                            <input type="text" name="answers[]" class="form-control" id="exampleInputName1" placeholder="Type answer">
                            <input type="radio" name="is_correct" value="2">
                            <input type="text" name="answers[]" class="form-control" id="exampleInputName1" placeholder="Type answer">
                            <input type="radio" name="is_correct" value="3">
                            <input type="text" name="answers[]" class="form-control" id="exampleInputName1" placeholder="Type answer">
                            <input type="radio" name="is_correct" value="4">
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a href="{{route('questions.index')}}" class="btn btn-default">Назад</a>
                    <button type="submit" class="btn btn-success pull-right">Добавить</button>
                </div>
                <!-- /.box-footer-->
            </div>
            <!-- /.box -->
            {{Form::close()}}
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection