@extends('admin.layout')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Questions
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Questions</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <a href="{{route('questions.create')}}" class="btn btn-success">Добавить вопрос</a>
                    </div>
                    <div style="overflow: auto;">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Вопрос</th>
                            {{--<th>Книга</th>--}}
                            {{--<th>Тема</th>--}}
                            {{--<th>Ответы</th>--}}
                            <th>Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($questions as $question)
                            <tr>
                                <td>{{$question->id}}</td>
                                <td>{{$question->question}}</td>
                                {{--<td>{{$question->description}}</td>--}}
                                {{--<td>{{$question->filename}}</td>--}}
                                <td>
                                    <a href="{{route('questions.edit', $question->id)}}" class="fa fa-pencil"></a>
                                    {{Form::open(['method' => 'delete', 'route' => ['questions.destroy', $question->id]])}}
                                    <button type="submit" class="delete" onclick="return confirm('Вы уверены, что хотите удалить вопрос?')">
                                        <i class="fa fa-remove"></i>
                                    </button>
                                    {{Form::close()}}
                                </td>
                            </tr>
                        @endforeach
                        </tfoot>
                    </table>
</div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection