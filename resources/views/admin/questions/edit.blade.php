@extends('admin.layout')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Редактирование вопроса
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
        {{Form::open(['method' => 'put', 'route' => ['questions.update', $question->id], 'files' => 'true'])}}
        <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Редактирование вопроса</h3>
                    @include('admin.errors')
                </div>
                <div class="box-body">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputDescription1">Question</label>
                            <textarea name="question" id="" cols="30" rows="7" class="form-control" id="exampleInputDescription1" placeholder="Question">{{$question->question}}</textarea>
                        </div>

                        <div class="form-group">
                            <label>Topic</label>
                            {{Form::select(
                                'topic_id',
                                $topics,
                                $question->topic->id,
                                ['class' => 'form-control select2']
                            )}}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputName1">Answers</label>
                            @foreach($answers as $answer)
                                <input type="text" name="answers[{{$loop->index}}][answer]" class="form-control" id="exampleInputName1" placeholder="Type answer" value="{{$answer->answer}}">
                                <input type="hidden" name="answers[{{$loop->index}}][answer_id]" value="{{$answer->id}}">
                                @if($answer->isCorrect)
                                    <input type="radio" name="is_correct" value="{{$loop->index}}" checked>
                                @else
                                    <input type="radio" name="is_correct" value="{{$loop->index}}">
                                @endif
                            @endforeach
                            {{--<input type="text" name="answers[]" class="form-control" id="exampleInputName1" placeholder="Type answer">--}}
                            {{--<input type="radio" name="is_correct" value="0" checked>--}}
                            {{--<input type="text" name="answers[]" class="form-control" id="exampleInputName1" placeholder="Type answer">--}}
                            {{--<input type="radio" name="is_correct" value="1">--}}
                            {{--<input type="text" name="answers[]" class="form-control" id="exampleInputName1" placeholder="Type answer">--}}
                            {{--<input type="radio" name="is_correct" value="2">--}}
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a href="{{route('questions.index')}}" class="btn btn-default">Назад</a>
                    <button type="submit" class="btn btn-warning pull-right">Изменить</button>
                </div>
                <!-- /.box-footer-->
            </div>
            <!-- /.box -->
            {{Form::close()}}
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection