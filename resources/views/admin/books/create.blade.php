@extends('admin.layout')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Добавить книгу
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
        {{Form::open(['method' => 'post', 'route' => 'books.store', 'files' => 'true'])}}
        <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Добавляем книгу</h3>
                    @include('admin.errors')
                </div>
                <div class="box-body">
                    <div class="col-md-6">

                        <div class="form-group">
                            <label for="exampleInputName1">Название книги</label>
                            <input type="text" name="name" value="{{old('name')}}" class="form-control" id="exampleInputName1" placeholder="Название книги">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPrice1">Цена книги</label>
                            <input type="text" name="price" value="{{old('price')}}" class="form-control" id="exampleInputPrice1" placeholder="Цена книги">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputDescription1">Описание книги</label>
                            <textarea name="description" id="" cols="30" rows="7" class="form-control" id="exampleInputDescription1" placeholder="Описание">{{old('description')}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile">Файл с книгой</label>
                            <input type="file" name="filename" id="exampleInputFile">

                            <p class="help-block">Для загрузки доступен только ".pdf" формат</p>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputFile">Лицевая картинка</label>
                            <input type="file" id="exampleInputFile" name="image">

                            <p class="help-block">Какое-нибудь уведомление о форматах..</p>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a href="{{route('books.index')}}" class="btn btn-default">Назад</a>
                    <button type="submit" class="btn btn-success pull-right">Добавить</button>
                </div>
                <!-- /.box-footer-->
            </div>
        <!-- /.box -->
        {{Form::close()}}
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection