@extends('admin.layout')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Редактирование книги
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
        {{Form::open(['method' => 'put', 'route' => ['books.update', $book->id], 'files' => 'true'])}}
        <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Редактирование книги</h3>
                    @include('admin.errors')
                </div>
                <div class="box-body">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputName1">Название книги</label>
                            <input type="text" name="name" value="{{$book->name}}" class="form-control" id="exampleInputName1" placeholder="Название">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPrice1">Цена книги</label>
                            <input type="text" name="price" value="{{$book->price}}" class="form-control" id="exampleInputPrice1" placeholder="Цена книги">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputDescription1">Описание книги</label>
                            <textarea name="description" id="" cols="30" rows="7" class="form-control" id="exampleInputDescription1" placeholder="Описание">{{$book->description}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile">Файл книги</label>
                            <input type="file" name="filename" id="exampleInputFile">

                            <p class="help-block">Для загрузки доступен только ".pdf" формат</p>
                        </div>
                        <div class="form-group">
                            <img src="{{$book->getImage()}}" alt="" class="img-responsive" width="200">
                            <label for="exampleInputFile">Лицевая картинка</label>
                            <input type="file" id="exampleInputFile" name="image">

                            <p class="help-block">Какое-нибудь уведомление о форматах..</p>
                        </div>

                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a href="{{route('books.index')}}" class="btn btn-default">Назад</a>
                    <button type="submit" class="btn btn-warning pull-right">Изменить</button>
                </div>
                <!-- /.box-footer-->
            </div>
            <!-- /.box -->
            {{Form::close()}}
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection