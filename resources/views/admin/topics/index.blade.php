@extends('admin.layout')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Topics
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            @if(session('status'))
                <div style="color: #ffffff; background-color: rgba(212, 30, 30, 0.5)">{{session('status')}}</div>
            @endif
            <!-- Default box -->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Topics</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <a href="{{route('topics.create')}}" class="btn btn-success">Добавить тему</a>
                    </div>
                    <div style="overflow: auto;">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Тема</th>
                            <th>Книга</th>
                            {{--<th>Название файла</th>--}}
                            <th>Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($topics as $topic)
                            <tr>
                                <td>{{$topic->id}}</td>
                                <td>{{$topic->topic}}</td>
                                <td>{{$topic->getBookName()}}</td>
                                {{--<td>{{$topic->filename}}</td>--}}
                                <td>
                                    <a href="{{route('topics.edit', $topic->id)}}" class="fa fa-pencil"></a>
                                    {{Form::open(['method' => 'delete', 'route' => ['topics.destroy', $topic->id]])}}
                                    <button type="submit" class="delete" onclick="return confirm('Вы уверены, что хотите удалить тему?')">
                                        <i class="fa fa-remove"></i>
                                    </button>
                                    {{Form::close()}}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
</div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection