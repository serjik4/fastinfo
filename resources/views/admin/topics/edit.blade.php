@extends('admin.layout')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Редактирование темы
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
        {{Form::open(['method' => 'put', 'route' => ['topics.update', $topic->id], 'files' => 'true'])}}
        <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Редактирование темы</h3>
                    @include('admin.errors')
                </div>
                <div class="box-body">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputName1">Название темы</label>
                            <input type="text" name="topic" value="{{$topic->topic}}" class="form-control" id="exampleInputName1" placeholder="Название темы">
                        </div>

                        <div class="form-group">
                            <label>Book</label>
                            {{Form::select(
                                'book_id',
                                $books,
                                $topic->book->id,
                                ['class' => 'form-control select2']
                            )}}
                        </div>

                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a href="{{route('topics.index')}}" class="btn btn-default">Назад</a>
                    <button type="submit" class="btn btn-warning pull-right">Изменить</button>
                </div>
                <!-- /.box-footer-->
            </div>
            <!-- /.box -->
            {{Form::close()}}
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection