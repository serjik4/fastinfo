@extends('admin.layout')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Редактирование пользователя
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
        {{Form::open(['method' => 'put', 'route' => ['users.update', $user->id], 'files' => 'true'])}}
        <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Редактирование пользователя</h3>
                    @include('admin.errors')
                </div>
                <div class="box-body">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputName1">Имя</label>
                            <input type="text" name="name" value="{{$user->name}}" class="form-control" id="exampleInputName1" placeholder="Имя">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputName2">Email</label>
                            <input type="text" name="email" value="{{$user->email}}" class="form-control" id="exampleInputName2" placeholder="Email">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputName3">Password</label>
                            <input type="password" name="password" value="" class="form-control" id="exampleInputName3" placeholder="Password">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Доступные книги</label>
                            {{Form::select(
                                'books[]',
                                $books,
                                $selected_books,
                                ['class' => 'form-control select2', 'data-placeholder' => 'Выбирите книги', 'style' => 'width: 100%;', 'multiple' => 'multiple']
                            )}}
                        </div>

                        <div class="form-group">
                            <label for="exampleInputName3">Факультет</label>
                            <input type="text" name="faculty" value="{{$user->faculty}}" class="form-control" id="exampleInputName3" placeholder="Факультет">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Коментарий</label>
                            <textarea name="comment" id="" cols="30" rows="10" class="form-control">{{$user->comment}}</textarea>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a href="{{route('users.index')}}" class="btn btn-default">Назад</a>
                    <button type="submit" class="btn btn-warning pull-right">Изменить</button>
                </div>
                <!-- /.box-footer-->
            </div>
            <!-- /.box -->
            {{Form::close()}}
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection