﻿@extends('layout')

@section('content')
    <section class="index">
        <!-- begin loader -->
        <div class="main-loader">
            <div class="helix">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
        <!-- end loader -->
        <div class="container index-wrap">
            <h1 class="index-text">Инвестируйте в своё развитие быстро и легко!</h1>
        </div>
        <!-- // container -->
    </section>
    <!-- // index -->
    <section class="index-desc">
        <div class="container">
            <h3 class="index-desc-title">
                Рады приветствовать вас!
            </h3>
            <div class="index-desc-subtitle">
                Вы попали на онлайн-ресурс, где можете пользоваться учебным материалом по данным преимуществам, что указаны ниже. Регистрируйтесь здесь на сайте и получайте доступ к учебному материалу.
            </div>
            <div class="index-btn">
                <button class="btn-create btn js-create">Зарегистрироваться</button>
                <!-- <button class="btn-callback btn">обратная связь</button> -->
            </div>
        </div>
        <!-- // container -->
    </section>
    <!-- // desc -->
    <section class="advantage">
        <div class="container">
            <h3 class="advantage-title">
                преимущества
            </h3>
            <div class="advantage-items">
                <div class="advantage-item">
                    <div class="advantage-item-img">
                        <img src="{{asset('img/conect.svg')}}" alt="#">
                    </div>
                    <div class="advantage-item-desc">
                        электронный доступ в любое время, в любом месте
                    </div>
                </div>
                <div class="advantage-item">
                    <div class="advantage-item-img">
                        <img src="{{asset('img/search.svg')}}" alt="#">
                    </div>
                    <div class="advantage-item-desc">
                        поиск по базе “Шпаргалка” на модуль
                    </div>
                </div>
                <div class="advantage-item">
                    <div class="advantage-item-img">
                        <img src="{{asset('img/test.svg')}}" alt="#">
                    </div>
                    <div class="advantage-item-desc">
                        проверь себя - пройди тестирование
                    </div>
                </div>
            </div>
        </div>
        <!-- // container -->
    </section>
    <!-- // adv -->
    <section class="books">
        <div class="container">
            <h3 class="books-title">
                Учебный материал
            </h3>
            <div class="books-items">
                @foreach($books as $book)
                    <div class="books-item">
                    <div class="books-item-img">
                        <img src="{{$book->getImage()}}" alt="">
                    </div>
                    <div class="books-item-title">
                        {{$book->name}}
                    </div>
                    <div class="books-item-desc">
                        {{$book->description}}
                    </div>
                    <button class="btn books-btn js-book-requst" data-bookname= "{{$book->name}}" >ПОЛУЧИТЬ ДОСТУП</button>
                </div>
                @endforeach
            </div>
        </div>
        <!-- // container -->
    </section>
@endsection