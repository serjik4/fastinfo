const book = new Book;
const ui = new UI;


//search input 
const searchinput = document.getElementById('js-search-input')

//search input event
searchinput.addEventListener('keyup', (e) => {
	const userText = e.target.value


	if(userText !== '' && userText.length > 2){
		//make http call
		book.getAnswer(userText)
		.then(data =>{
			if(data.data.message === 'not found'){
				//show alert
			}else{
				//show result
				ui.showResult(data.data)
			}
		})
	}
})