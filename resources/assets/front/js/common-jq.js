$(document).ready(function(){
//////////////////////modal-open-close humburger/////////////////////////
  $(document).on('touchend', '.nav-mob-humburger', function(){
    if($(this).hasClass('open')){
      $(this).removeClass('open')
      $('.nav-mob').removeClass('open')
      $('.header').removeClass('open')
      $('body').removeClass('hid-mainden')
    }else{
      $('body').addClass('hidden-main')
      $(this).addClass('open')
      $('.nav-mob').addClass('open')
      $('.header').addClass('open')
    }
  });
  $(document).on('click', '.js-login', function(e){
    e.preventDefault()
    $('.js-modal-login').addClass('show')
  })
    $(document).on('click', '.js-create', function(e){
    e.preventDefault()
    $('.js-modal-reg').addClass('show')
  })
    $(document).on('click', '.modal-back', function(e){
    $('.modal-wrap').removeClass('show')
  })

/////////////////////js-btn-tab-active///////////////////////
  $(document).on('click', '.js-btn-tab', function(e){
    let data = $(this).data('atributte')
    $('.test-page-content').removeClass('active')
    $(`.test-page-content[data-atributte=${data}]`).addClass('active')
    $('.js-btn-tab').removeClass('active')
    $(this).addClass('active')
  })
/////////////////input search-input////////////////////////
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $(".dropdown-menu li").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
  $('.js-select-search-btn').on('click', function(e){
    e.preventDefault()
    let data = $(this).data('category')
    $('.js-select-search-input').text(data)
  })
///////////////////end-js/////////////////////////
})