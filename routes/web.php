<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//URL::forceScheme('https');

Route::get('/', 'HomeController@index')->name('index');

//Auth::routes();

// Authentication Routes...
//Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login')->name('login');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
//Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register')->name('register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

//Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'admin'], function () {
    Route::get('/', 'DashboardController@index')->name('admin.dashboard');
    Route::resource('/books', 'BooksController')->except(['show']);
    Route::resource('/questions', 'QuestionsController')->except(['show']);
    Route::resource('/topics', 'TopicsController')->except(['show']);
    Route::resource('/users', 'UsersController')->except(['show']);
});

Route::group(['middleware' => 'auth', 'namespace' => 'Personal'], function (){
    Route::get('/dashboard', 'DashboardController@index')->name('personal.dashboard');
    Route::get('/dashboard/{id}', 'DashboardController@showTestsPage')->name('personal.tests');
    Route::get('/book/{filename}', 'DashboardController@showBook')->name('show.book');
});