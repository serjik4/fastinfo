

$(document).ready(function(){
//////////////////////modal-open-close humburger/////////////////////////
  $(document).on('touchend', '.nav-mob-humburger', function(){
    if($(this).hasClass('open')){
      $(this).removeClass('open')
      $('.nav-mob').removeClass('open')
      $('.header').removeClass('open')
      $('body').removeClass('hidden-main')
    }else{
      $('body').addClass('hidden-main')
      $(this).addClass('open')
      $('.nav-mob').addClass('open')
      $('.header').addClass('open')
    }
  });
  $(document).on('click', '.js-login', function(e){
    e.preventDefault()
    $('.js-modal-login').addClass('show')
  })
    $(document).on('click', '.js-create', function(e){
    e.preventDefault()
    $('.js-modal-reg').addClass('show')
  })
    $(document).on('click', '.modal-back', function(e){
    $('.modal-wrap').removeClass('show')
  })
///////////////////js-book-requst//////////////////////////
$(document).on('click', '.js-book-requst', function(e){
  let name = $(this).data('bookname')
  $('.js-modal-desc-title-requst').text(name)
  $('.js-modal-requst').addClass('show')
})
/////////////////////js-btn-tab-active///////////////////////
  $(document).on('click', '.js-btn-tab', function(e){
    let data = $(this).data('atributte')
    $('.test-page-content').removeClass('active')
    $(`.test-page-content[data-atributte=${data}]`).addClass('active')
    let bookid = $('#js-search-input').data('bookid')
    $('.js-btn-tab').removeClass('active')
    $(this).addClass('active')
    if(data === 'testModul'){
    $('.main-loader').addClass('active')
      $.ajax({
      type: 'post',
      url: '/api/questions',
      data: {'book_id': bookid},
      success: function(data){
        $('.main-loader').removeClass('active')
        dataModule = data
        moduleCount = 0
        greenAnswer = 0
        redAnswer = 0
        checkAnswer = 0
        displayModuleTest()
      },
      error: function(err){
        console.log(err)
      }
    })
    }
  }) 
  var moduleCount = 0
  var greenAnswer = 0
  var redAnswer = 0
  var checkAnswer = 0
  var dataModule
  function displayModuleTest(){
    arr = dataModule[moduleCount].answers
    let listHtml = ''
    for (var i = 0; i < arr.length; i++){
      listHtml += `<li class="test-page-content-li"><input value="ssssss" class="js-test-module-input" type="radio" name="Q" id="AM${i}" data-curecct="${dataModule[moduleCount].correct_answer}" data-name="${arr[i].answer}"><label for="AM${i}">${arr[i].answer}</label></li>`
    }
    $('.js-module-content').html(`<div class="test-page-content-test">
  <div class="test-page-content-test-q">${dataModule[moduleCount].question}</div>
<ul>
  ${listHtml}
</ul>
</div>`)
  }
  $('.js-module-test-next').on('click', function(){
  	let next = false
  	$(".js-test-module-input").each(function(){
  		if($(this).prop("checked")){
  			next = true
  			let currectAnswer = $(this).data('curecct')
  			let valueAnswer = $(this).data('name')
  			if(currectAnswer === valueAnswer){
  				$(this).parent().addClass('green')
          $(this).prop("checked", false)
          greenAnswer++
          console.log(`green: ${greenAnswer}`)
  			}else{
  				$(this).parent().addClass('red')
          $(this).prop("checked", false)
          redAnswer++
          console.log(`red: ${redAnswer}`)
  			}
  		}
  	})
  	setTimeout(function(){
  	if(next){
  		if(moduleCount < 29 && next){
  		moduleCount += 1
  			displayModuleTest() 
        // next = false
      }else{
      displayModuleResult()
      }
    }else{
      return
    }
      
    }, 1500)
  })

  $('.js-module-test-check').on('click', function(){
        $(".js-test-module-input").each(function(){
        let currectAnswer = $(this).data('curecct')
        let valueAnswer = $(this).data('name')
        if(currectAnswer === valueAnswer){
          $(this).parent().addClass('green')
          $(this).prop("checked", true)
          checkAnswer++
          console.log(`check ${checkAnswer}`)
        }
    })
  })

  function displayModuleResult(){
    $('.js-module-content').html(`
    <div class="module-test-finished-wrap">
      <div class="module-test-finished-title">
        Ваш результать:
      </div>
      <div class="module-test-finished-red">
        не правильные ответы: <span>${redAnswer}</span>
      </div>
      <div class="module-test-finished-check">
        Использовали подсказку: <span>${checkAnswer}</span> раз
      </div>
      <div class="module-test-finished-green">
        Правильные ответы: <span>${greenAnswer}</span>
      </div>
    </div>
    `)
  }
/////////////////input search-input////////////////////////
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $(".dropdown-menu li").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
  var testCount = 0
  var datatest
  $('.js-select-search-btn').on('click', function(e){
    $('.main-loader').addClass('active')
    e.preventDefault()
    let name = $(this).data('name')
    $('.js-select-search-input').text(name)
    let data = $(this).data('category')
    $.ajax({
      type: 'post',
      url: '/api/questions-topic',
      data: {'topic_id': data},
      success: function(data){
        $('.main-loader').removeClass('active')
        datatest = data
        testCount = 0
        $('.js-test-page-content-test-counter-all').text(data.length)
        $('.js-test-page-content-test-counter').text(testCount)  
        displayTest()
        console.log(data)
      },
      error: function(err){
        console.log(err)
      }
    })

  function displayTest(){
    arr = datatest[testCount].answers
    console.log(testCount)
    let listHtml = ''
    for (var i = 0; i < arr.length; i++){
      listHtml += `<li class="test-page-content-li"><input value="ssssss" class="js-test-input" type="radio" name="Q" id="A${i}" data-curecct="${datatest[testCount].correct_answer}" data-name="${arr[i].answer}"><label for="A${i}">${arr[i].answer}</label></li>`
    }
    $('.js-test-page-content-test').html(`<div class="test-page-content-test">
  <div class="test-page-content-test-q">${datatest[testCount].question}</div>
<ul>
  ${listHtml}
</ul>
</div>`)
  }
  $('.js-test-next').on('click', function(){
  	let next = false
  	$(".js-test-input").each(function(){
  		if($(this).prop("checked")){
  			let currectAnswer = $(this).data('curecct')
  			let valueAnswer = $(this).data('name')
  			if(currectAnswer === valueAnswer){
  				$(this).parent().addClass('green')
          $(this).prop("checked", false)
          next = true
  			}else{
  				$(this).parent().addClass('red')
          $(this).prop("checked", false)
  			}
  		}
  	})
  	setTimeout(function(){
  	if(next){
  		if(testCount < (datatest.length - 1) && next){
        testCount += 1
      $('.js-test-page-content-test-counter').text(testCount)
  			displayTest() 
        next = false

      }else{
      $('.js-test-page-content-test-counter').text(testCount + 1)
      displayTestResult()
      }
    }else{
      return
    }
      
    }, 1500)
  })

  })

    $('.js-test-check').on('click', function(){
        $(".js-test-input").each(function(){
        let currectAnswer = $(this).data('curecct')
        let valueAnswer = $(this).data('name')
        if(currectAnswer === valueAnswer){
          $(this).parent().addClass('green')
          $(this).prop("checked", true)
        }
    })
  })

  function displayTestResult(){
    $('.js-test-page-content-test').html(`<div class="test-finish">вопросы по этой теме закончились</div>`)
  }
//////////////////////////////////////////

  $('#js-search-input').keyup(function(){
  if($('#js-search-input').val().length > 2){
    // console.log('run')
    let data = $('#js-search-input').val()
    let bookid = $('#js-search-input').data('bookid')
    $.ajax({
      type: 'post',
      url: '/api/search',
      data: {'search': data, 'bookid': bookid},
      success: function(data){
        dispalySearch(data)
      },
      error: function(err){
        console.log(err)
      } 
    })
  }
})


  function dispalySearch (data){
    $('.js-search-content').html('')
    if(data.length > 0){
        data.forEach(function(item, i, arr) {
          $('.js-search-content').append(`<div class="test-page-conten-search">
            <span class="test-page-conten-search-q">${item.question}</span>
            <span class="test-page-conten-search-a">${item.answer}</span>
            </div>`)
});
    }else{
      $('.js-search-content').append(`<div class="test-page-conten-search-empty">ничего не найдено</div>`)
    }


  }
///////////////////end-js/////////////////////////
})