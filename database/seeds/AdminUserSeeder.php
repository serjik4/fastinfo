<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Ammar Kiravan',
            'email' => 'ammar.kira@gmail.com',
            'password' => bcrypt('fastkiravan'),
            'is_admin' => 1,
            'session_id' => 'session_id'
        ]);
    }
}
