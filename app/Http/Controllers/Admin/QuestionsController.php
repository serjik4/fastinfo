<?php

namespace App\Http\Controllers\Admin;

use App\Answer;
use App\Question;
use App\Rules\NotEmptyAnswersArray;
use App\Topic;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QuestionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = Question::get()->all();
        //dd($questions);
        return view('admin.questions.index', [
            'questions' => $questions
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $topics = Topic::pluck('topic', 'id')->all();
        //dd($topics);
        return view('admin.questions.create', [
            'topics' => $topics
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->get('answers'));
        $this->validate($request, [
            'question' => 'required',
            'topic_id' => 'required',
            //'answers' => ['required', new NotEmptyAnswersArray()]
        ]);
        //dd(Topic::find($request->get('topic_id'))->book->id);
        $question = Question::add($request->except(['answers', 'is_correct']));
        $book_id = Topic::find($request->get('topic_id'))->book->id;
        $question->setBookId($book_id);
        foreach ($request->get('answers') as $key => $answer) {
            //dd($answer);
            if($answer != null){
                $answer = Answer::add(['answer' => $answer]);
                $answer->setTopicId($request->get('topic_id'));
                $answer->setQuestionId($question->id);
                if($key == $request->get('is_correct')){
                    $answer->isCorrect();
                } else {
                    $answer->isNotCorrect();
                }
            }
        }

        return redirect()->route('questions.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $question = Question::find($id);
        $answers = $question->answers()->get()->all();
        $topics = Topic::pluck('topic', 'id')->all();
        //$topics;
        //$selectTopic;
        //dd($answers);
        return view('admin.questions.edit', [
            'question' => $question,
            'answers' => $answers,
            'topics' => $topics
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());
        $this->validate($request, [
            'question' => 'required',
            'topic_id' => 'required',
//            'answers' => ['required', new NotEmptyAnswersArray()]
        ]);

        $question = Question::find($id);
        $question->edit($request->except(['answers', 'is_correct']));
        $book_id = Topic::find($request->get('topic_id'))->book->id;
        $question->setBookId($book_id);

        foreach ($request->get('answers') as $key => $answer) {
            //dd($answer);
            if($answer != null){
                $answer = Answer::edit($answer['answer_id'], ['answer' => $answer['answer']]);
                $answer->setTopicId($request->get('topic_id'));
                $answer->setQuestionId($question->id);
                if($key == $request->get('is_correct')){
                    $answer->isCorrect();
                } else {
                    $answer->isNotCorrect();
                }
            }
        }

        return redirect()->route('questions.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $question = Question::find($id);
        $question->answers()->delete();
        $question->delete();

        return redirect()->route('questions.index');
    }
}
