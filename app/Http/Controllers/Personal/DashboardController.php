<?php

namespace App\Http\Controllers\Personal;

use App\Book;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class DashboardController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $available_books = $user->books()->get();
        //dd($available_books);
        $books = Book::all();
//        dd($available_books);
        return view('personal.dashboard',  [
            'available_books' => $available_books,
            'books' => $books
        ]);
    }

    public function showTestsPage($id)
    {
        $book = Book::find($id);
        $topics = $book->topics()->get();
//        dd($topics->get()->all());
    	return view('personal.testspage', [
    	    'book' => $book,
            'topics' => $topics
        ]);
    }

    public function showBook(Request $request)
    {
        return response()->file('storage/books/' . $request->route()->parameters()['filename']);
    }
}
