<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Question;

class ApiController extends Controller
{
    public function search(Request $request)
    {
    	$questions = Question::where('question', 'LIKE', '%' . $request->get('search') . '%')->where('book_id', $request->get('bookid'))->get();
        //$questions = Question::where('question', 'LIKE', '%как%')->get();
    	$response = [];
    	for ($i = 0; $i < count($questions);$i++) {
            $response[$i] = $questions[$i]->toArray();
            $response[$i]['answer'] = $questions[$i]->answers()->where('isCorrect', '1')->first()->answer;
        }

    	return response()->json($response);
    }

    /*get questions for module test*/
    public function getModuleQuestions(Request $request)
    {
        $count = Question::where('book_id', $request->get('book_id'))->inRandomOrder()->count();

        if($count < 30) {
            $questions = Question::where('book_id', $request->get('book_id'))->inRandomOrder()->get();
        } else {
            $questions = Question::where('book_id', $request->get('book_id'))->inRandomOrder()->take(30)->get();
        }
        $response = [];

        for ($i = 0; $i <= (count($questions) - 1); $i++) {
            $response[$i] = $questions[$i];
            $response[$i]['answers'] = $questions[$i]->answers;
            $response[$i]['correct_answer'] = $questions[$i]->answers()->where('isCorrect', 1)->first()->answer;
        }

        return response()->json($response);
    }

    public function getQuestionsFromTopic(Request $request)
    {
//        dd('hello');
        $questions = Question::where('topic_id', $request->get('topic_id'))->get();
//        $questions = Question::where('topic_id', 10)->get()->toArray();
        $response = [];
        for ($i = 0; $i < count($questions); $i++) {
            $response[$i] = $questions[$i];
            $response[$i]['answers'] = $questions[$i]->answers;
            $response[$i]['correct_answer'] = $questions[$i]->answers()->where('isCorrect', 1)->first()->answer;
        }

        //dd($questions);
        return response()->json($response);
    }
}
