<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use App\Question;
use App\Topic;

class Book extends Model
{
    public $fillable = ['name', 'description', 'price'];

    public function users()
    {
        return $this->belongsToMany(
            User::class,
            'book_user',
            'book_id',
            'user_id'
        );
    }

    public function topics()
    {
        return $this->hasMany(Topic::class);
    }

    public function questions()
    {
        return $this->hasManyThrough(Question::class, Topic::class);
    }

    public static function add($fields)
    {
        $book = new Book();
        $book->fill($fields);
        $book->save();

        return $book;
    }

    public function uploadFile($file)
    {
        if($file == null){return;}
        if($this->filename != null)
        {
            $this->removeBook();
        }
        $filename = str_random(12) . '.' . $file->extension();
        $file->storeAs('public/books/', $filename);
        $this->filename = $filename;
        $this->save();
    }

    public function edit($fields)
    {
        $this->fill($fields);
        $this->save();
    }

    public function removeImage()
    {
        if ($this->image != null)
        {
            Storage::delete('/public/uploads/' . $this->image);
        }
    }

    public function removeBook()
    {
        if ($this->filename != null)
        {
            Storage::delete('/public/books/' . $this->filename);
        }
    }

    public function remove()
    {
//        dump(get_class_methods($this->topics));
//        dd($this->questions()->get());
        $this->removeQuestions();
        $this->removeImage();
        $this->removeTopics();
        $this->removeBook();
        $this->delete();
    }

    public function removeTopics()
    {
        $this->topics()->delete();
    }

    public function removeQuestions()
    {
        $this->questions()->get()->each(function ($question) {
            $question->delete();
        });
    }

    public function uploadImage($image)
    {
        if($image == null) { return; }
        if ($this->image != null)
            $this->removeImage();
        $filename = str_random(10) . '.' . $image->extension();
        $image->storeAs('public/uploads', $filename);
        $this->image = $filename;
        $this->save();
    }

    public function getImage()
    {
        if($this->image == null)
        {
            return '/images/no-img.png';
        }

        return '/storage/uploads/' . $this->image;
    }
}
