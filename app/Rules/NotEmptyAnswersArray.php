<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class NotEmptyAnswersArray implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute,$value)
    {
        foreach ($value as $key => $answer){
            if($answer['answer'] == '' || $answer['answer_id'] == ''){
                return false;
            }
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Add answers to question';
    }
}
