<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Question;
use App\Book;

class Topic extends Model
{
    public $fillable = ['topic', 'book_id'];

    public function questions()
    {
        return $this->hasMany(Question::class);
    }

    public function book()
    {
        return $this->belongsTo(Book::class);
    }

    public static function add($fields)
    {
        $topic = new Topic();
        $topic->fill($fields);
        $topic->save();

        return $topic;
    }

    public function edit($fields)
    {
        $this->fill($fields);
        $this->save();
    }

    public function getBookName()
    {
        if($this->book != null){
            return $this->book->name;
        }

        return false;
    }

    public function remove()
    {
        $this->removeQuestions();
        $this->delete();
    }

    public function removeQuestions()
    {
        $this->questions()->delete();
    }
}
