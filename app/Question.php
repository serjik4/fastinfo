<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Topic;
use App\Answer;

class Question extends Model
{
    public $fillable = ['question', 'topic_id'];

    public function topic()
    {
        return $this->belongsTo(Topic::class);
    }

    public function answers()
    {
        return $this->hasMany(Answer::class);
    }

    public static function add($fields)
    {
        $question = new Question();
        $question->fill($fields);
        $question->save();

        return $question;
    }

    public function setBookId($book_id)
    {
        $this->book_id = $book_id;
        $this->save();
    }

    public function edit($fields)
    {
        $this->fill($fields);
        $this->save();
    }
}
