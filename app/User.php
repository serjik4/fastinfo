<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Book;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Mail;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'faculty', 'phone', 'comment'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function books()
    {
        return $this->belongsToMany(
            Book::class,
            'book_user',
            'user_id',
            'book_id'
        );
    }

    public function setBooks($ids)
    {
        $this->books()->sync($ids);
    }

    public static function add($fields)
    {
        $user = new User();
        $user->fill($fields);
        $user->save();

        return $user;
    }

    public function edit($fields)
    {
        $this->fill($fields);
        $this->save();
    }

    public function generatePassword($password)
    {
        if ($password != null)
        {
            $this->password = bcrypt($password);
            $this->save();
        }
    }

    public function sendEmailWithBooks($book)
    {
        Mail::send('email.open_access_email', ['book' => $book], function ($m) {
            $m->from('info@fastinfo.com.ua', 'FastInfo');
            $m->to($this->email)->subject('Доступ открыт');
        });
    }
}
