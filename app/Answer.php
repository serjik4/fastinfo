<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Question;

class Answer extends Model
{
    public $fillable = ['answer', 'question_id'];

    public function question()
    {
        return $this->belongsTo(Question::class);
    }

    public static function add($fields)
    {
        $answer = new Answer();
        $answer->fill($fields);
        $answer->save();

        return $answer;
    }

    public static function edit($id, $fields)
    {
        $answer = Answer::find($id);
        $answer->fill($fields);
        $answer->save();

        return $answer;
    }

    public function isCorrect()
    {
        $this->isCorrect = 1;
        $this->save();
    }

    public function isNotCorrect()
    {
        $this->isCorrect = 0;
        $this->save();
    }

    public function setTopicId($topic_id)
    {
        $this->topic_id = $topic_id;
        $this->save();
    }

    public function setQuestionId($question_id)
    {
        $this->question_id = $question_id;
        $this->save();
    }
}
